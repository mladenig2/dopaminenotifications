import { TextModel } from './models/text.model';
import { PromotionModel } from './models/promotion.model';
import { BonusModel } from './models/bonus.model';
import { CONFIGS } from './../config/config';
import { BaseNotification } from './models/notification.model';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  public bonusList: Subject<void> = new Subject();
  public promotionList: Subject<void> = new Subject();
  public textList: Subject<void> = new Subject();

  constructor(
    private readonly http: HttpClient
  ) {}

  public isBonusUpdated(): Observable<void> {
    return this.bonusList.asObservable();
  }

  public isPromotionUpdated(): Observable<void> {
    return this.promotionList.asObservable();
  }

  public isTextUpdated(): Observable<void> {
    return this.textList.asObservable();
  }

  public getAllNotifications(): Observable<BaseNotification[]> {
    return this.http.get<BaseNotification[]>(
      `${CONFIGS.API_DOMAIN_NAME}/api/notifications`
    );
  }

  public getBonuses(): Observable<BonusModel[]> {
    return this.http.get<BonusModel[]>(
      `${CONFIGS.API_DOMAIN_NAME}/api/notifications/bonuses`
    );
  }

  public getPromotions(): Observable<PromotionModel[]> {
    return this.http.get<PromotionModel[]>(
      `${CONFIGS.API_DOMAIN_NAME}/api/notifications/promotions`
    );
  }

  public getTexts(): Observable<TextModel[]> {
    return this.http.get<TextModel[]>(
      `${CONFIGS.API_DOMAIN_NAME}/api/notifications/texts`
    );
  }

  public checkNotification(id: number) {
    return this.http.put<PromotionModel | TextModel | BonusModel>(
      `${CONFIGS.API_DOMAIN_NAME}/api/notifications/${id}`, null
    );
  }
}
