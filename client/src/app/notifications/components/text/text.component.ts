import { TextModel } from './../../models/text.model';
import { Component, OnInit, Input } from '@angular/core';
import { NotificationsService } from '../../notifications.service';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit {
  @Input() text: TextModel;
  texts;

  constructor(
    private readonly notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.notificationsService.isTextUpdated().subscribe(() => {
      this.loadTexts();
    });
    this.loadTexts();
  }

  public checkNotification(id: number) {
    this.notificationsService.checkNotification(id).subscribe(
      (data: TextModel) => {
        this.text = data;
      }
    );
  }

  private loadTexts(): void {
    this.notificationsService.getTexts().subscribe(
      data => {
        this.texts = data;
      }
    );
  }
}
