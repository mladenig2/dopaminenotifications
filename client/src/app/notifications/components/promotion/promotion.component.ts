import { Component, OnInit, Input } from '@angular/core';
import { PromotionModel } from '../../models/promotion.model';
import { NotificationsService } from '../../notifications.service';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.css']
})
export class PromotionComponent implements OnInit {
  @Input() promotion: PromotionModel;
  promotions;

  constructor(
    private readonly notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.notificationsService.isPromotionUpdated().subscribe(() => {
      this.loadPromotions();
    });
    this.loadPromotions();
  }

  public checkNotification(id: number) {
    this.notificationsService.checkNotification(id).subscribe(
      (data: PromotionModel) => {
        this.promotion = data;
      }
    );
  }

  private loadPromotions(): void {
    this.notificationsService.getPromotions().subscribe(
      data => {
        this.promotions = data;
      }
    );
  }
}
