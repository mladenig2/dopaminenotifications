import { Component, OnInit, Input } from '@angular/core';
import { BonusModel } from '../../models/bonus.model';
import { NotificationsService } from '../../notifications.service';

@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.css']
})
export class BonusComponent implements OnInit {
  @Input() bonus: BonusModel;
  bonuses;

  constructor(
    private readonly notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.notificationsService.isBonusUpdated().subscribe(() => {
      this.loadBonuses();
    });
    this.loadBonuses();
  }

  public checkNotification(id: number) {
    this.notificationsService.checkNotification(id).subscribe(
      (data: BonusModel) => {
        this.bonus = data;
      }
    );
    this.loadBonuses();
  }

  private loadBonuses(): void {
    this.notificationsService.getBonuses().subscribe(
      data => {
        this.bonuses = data;
      }
    );
  }
}
