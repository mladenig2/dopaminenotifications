import { NotificationType } from './notification.enum';

export class PromotionModel {
  public id: number;
  public type: NotificationType;
  public image: string;
  public title: string;
  public link: string;
  public isChecked: boolean;
}
