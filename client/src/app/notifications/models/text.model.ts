import { NotificationType } from './notification.enum';

export class TextModel {
  public id: number;
  public type: NotificationType;
  public title: string;
  public text: string;
  public expires: number;
  public isChecked: boolean;
}
