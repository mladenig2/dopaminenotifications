import { NotificationType } from './notification.enum';

export class BaseNotification {
  public id: number;

  public type: NotificationType;

  public title: string;

  public isChecked: boolean;

  public createdAt: number;

  public text?: string;

  public expires?: number;

  public requirement?: string;

  public image?: string;

  public link?: string;
}
