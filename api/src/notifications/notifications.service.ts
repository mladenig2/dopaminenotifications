import { TextDTO } from './../common/DTOs/text/text-notification.dto';
import { PromotionDTO } from './../common/DTOs/promotion/promotion-notification.dto';
import { BonusDTO } from './../common/DTOs/bonus/bonus-notification.dto';
import { Text } from './../database/entities/text-notification.entity';
import { Bonus } from './../database/entities/bonus-notification.entity';
import { SystemError } from './../common/exception/system.error';
import { BaseNotificationDTO } from './../common/DTOs/base/base-notification.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseNotification } from '../database/entities/base-notification.entity';
import { Repository } from 'typeorm';
import { Promotion } from '../database/entities/promotion-notification.entity';
import { NotificationType } from '../common/enums/notification-type.enum';

@Injectable()
export class NotificationsService {
  constructor(
    @InjectRepository(BaseNotification) private readonly notificationsRepo: Repository<BaseNotification>,
    @InjectRepository(Bonus) private readonly bonusNotificationsRepo: Repository<Bonus>,
    @InjectRepository(Text) private readonly textNotificationsRepo: Repository<Text>,
    @InjectRepository(Promotion) private readonly promotionNotificationsRepo: Repository<Promotion>,
  ) {}

  public async getNotifications(): Promise<BaseNotificationDTO[]> {
    const notifications =  await this.notificationsRepo.find({
      where: { isChecked: false },
      order: { createdAt: 'DESC' },
    });

    if (!notifications) {
      throw new SystemError('No notifications found', 404);
    }

    return notifications;
  }

  public async getBonuses(): Promise<BonusDTO[]> {
    return await this.bonusNotificationsRepo.find({
      where: {
        isChecked: false,
        type: NotificationType.Bonus,
      },
      order: { createdAt: 'DESC' },
    });
  }

  public async getPromotions(): Promise<PromotionDTO[]> {
    return this.promotionNotificationsRepo.find({
      where: {
        isChecked: false,
        type: NotificationType.Promotion,
      },
      order: { createdAt: 'DESC' },
    });
  }

  public async getTexts(): Promise<TextDTO[]> {
    return this.textNotificationsRepo.find({
      where: {
        isChecked: false,
        type: NotificationType.Text,
      },
      order: { createdAt: 'DESC' },
    });
  }

  public async createNotification(notification: BaseNotificationDTO): Promise<BaseNotificationDTO> {

    if (notification.link || notification.image) {
      const newNotification = this.promotionNotificationsRepo.create();
      newNotification.image = notification.image;
      newNotification.link = notification.link;
      newNotification.title = notification.title;
      newNotification.type = NotificationType.Promotion;

      return this.promotionNotificationsRepo.save(newNotification);
    } else if (notification.requirement) {
      const newNotification = this.bonusNotificationsRepo.create();
      newNotification.requirement = notification.requirement;
      newNotification.title = notification.title;
      newNotification.type = NotificationType.Bonus;

      return this.bonusNotificationsRepo.save(newNotification);
    } else if (notification.text) {
      const newNotification = this.textNotificationsRepo.create();
      newNotification.text = notification.text;
      newNotification.title = notification.title;
      newNotification.type = NotificationType.Text;

      return this.textNotificationsRepo.save(newNotification);
    } else {
      throw new SystemError('Something went wrong.', 400);
    }
  }

  public async checkNotification(id: number): Promise<BaseNotificationDTO> {
    const target = this.findById(id);
    (await target).isChecked = true;

    return this.notificationsRepo.save(await target);
  }

  private async findById(id: number): Promise<BaseNotificationDTO> {
    const target = await this.notificationsRepo.findOne({
      id,
    });

    return target;
  }
}
