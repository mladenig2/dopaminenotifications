import { TextDTO } from './../common/DTOs/text/text-notification.dto';
import { PromotionDTO } from './../common/DTOs/promotion/promotion-notification.dto';
import { BonusDTO } from './../common/DTOs/bonus/bonus-notification.dto';
import { BaseNotificationDTO } from './../common/DTOs/base/base-notification.dto';
import { SystemErrorFilter } from './../common/filter/error.filter';
import { NotificationsService } from './notifications.service';
import { Controller, Get, UseFilters, Post, Param, Body, Put } from '@nestjs/common';

@Controller('notifications')
export class NotificationsController {
  constructor(
    private readonly notificationsService: NotificationsService,
  ) {}

  @Get()
  @UseFilters(SystemErrorFilter)
  public async getAllNotifications(): Promise<BaseNotificationDTO[]> {
    return await this.notificationsService.getNotifications();
  }

  @Get('bonuses')
  @UseFilters(SystemErrorFilter)
  public async getBonuses(): Promise<BonusDTO[]> {
    return await this.notificationsService.getBonuses();
  }

  @Get('promotions')
  @UseFilters(SystemErrorFilter)
  public async getPromotions(): Promise<PromotionDTO[]> {
    return await this.notificationsService.getPromotions();
  }

  @Get('texts')
  @UseFilters(SystemErrorFilter)
  public async getTexts(): Promise<TextDTO[]> {
    return await this.notificationsService.getTexts();
  }

  @Post()
  @UseFilters(SystemErrorFilter)
  public async createNotification(
    @Body() createNotification: BaseNotificationDTO,
  ) {
    return await this.notificationsService.createNotification(createNotification);
  }

  @Put(':id')
  @UseFilters(SystemErrorFilter)
  public async checkNotification(
    @Param('id') id: number,
  ) {
    return this.notificationsService.checkNotification(id);
  }
}
