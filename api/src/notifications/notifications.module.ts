import { Promotion } from './../database/entities/promotion-notification.entity';
import { Bonus } from './../database/entities/bonus-notification.entity';
import { Text } from './../database/entities/text-notification.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { NotificationsService } from './notifications.service';
import { NotificationsController } from './notifications.controller';
import { BaseNotification } from '../database/entities/base-notification.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BaseNotification,
      Text,
      Bonus,
      Promotion,
    ]),
  ],
  providers: [NotificationsService],
  controllers: [NotificationsController],
  exports: [NotificationsService],
})
export class NotificationsModule {}
