import { ConfigModule } from '../config/config.module';
import {
  Module,
  Global,
} from '@nestjs/common';

@Global()
@Module({
  imports: [
    ConfigModule,
  ],
  exports: [
    ConfigModule,
  ],
})
export class CoreModule {}
