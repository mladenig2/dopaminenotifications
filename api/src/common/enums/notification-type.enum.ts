export enum NotificationType {
  Text = 'Text',
  Bonus = 'Bonus',
  Promotion = 'Promotion',
}
