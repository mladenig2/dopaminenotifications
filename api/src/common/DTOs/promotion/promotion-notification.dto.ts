import { NotificationType } from './../../enums/notification-type.enum';

export class PromotionDTO {
  public id: number;
  public type: NotificationType;
  public image: string;
  public title: string;
  public link: string;
  public isChecked: boolean;
}
