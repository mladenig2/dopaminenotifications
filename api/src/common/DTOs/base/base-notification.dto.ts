import { NotificationType } from './../../enums/notification-type.enum';

export class BaseNotificationDTO {
  public id: number;
  public type: NotificationType;
  public title: string;
  public isChecked: boolean;
  public createdAt: number;
  public text?: string;
  public expires?: number;
  public requirement?: string;
  public image?: string;
  public link?: string;
}
