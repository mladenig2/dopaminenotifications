import { NotificationType } from './../../enums/notification-type.enum';

export class TextDTO {
  public id: number;
  public type: NotificationType;
  public title: string;
  public text: string;
  public expires: number;
  public isChecked: boolean;
}
