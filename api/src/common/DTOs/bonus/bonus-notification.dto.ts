import { NotificationType } from './../../enums/notification-type.enum';

export class BonusDTO {
  public id: number;
  public type: NotificationType;
  public title: string;
  public requirement: string;
  public expires: number;
  public isChecked: boolean;
}
