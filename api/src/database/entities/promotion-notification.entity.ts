import { Column, ChildEntity } from 'typeorm';
import { BaseNotification } from './base-notification.entity';

@ChildEntity()
export class Promotion extends BaseNotification {
  @Column('nvarchar', {nullable: true})
  public image: string;

  @Column('nvarchar', {nullable: true})
  public link: string;
}
