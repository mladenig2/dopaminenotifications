import { Column, ChildEntity } from 'typeorm';
import { BaseNotification } from './base-notification.entity';

@ChildEntity()
export class Text extends BaseNotification {
  @Column('nvarchar', {nullable: false})
  public text: string;

  @Column('nvarchar', {default: 3600})
  public expires: number;
}
