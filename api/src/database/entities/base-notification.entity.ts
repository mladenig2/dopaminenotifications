import { PrimaryGeneratedColumn, Column, CreateDateColumn, Entity, TableInheritance } from 'typeorm';
import { NotificationType } from '../../common/enums/notification-type.enum';

@Entity()
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
export class BaseNotification {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({type: 'enum', enum: NotificationType})
  public type: NotificationType;

  @Column('nvarchar', {nullable: false})
  public title: string;

  @Column({type: 'boolean', default: false})
  public isChecked: boolean;

  @CreateDateColumn({type: 'timestamp'})
  public createdAt: number;
}
