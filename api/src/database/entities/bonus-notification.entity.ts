import { Column, ChildEntity } from 'typeorm';
import { BaseNotification } from './base-notification.entity';

@ChildEntity()
export class Bonus extends BaseNotification {
  @Column('nvarchar', {nullable: false})
  public requirement: string;

  @Column('nvarchar', {default: 3600})
  public expires: number;
}
