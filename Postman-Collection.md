{
	"info": {
		"_postman_id": "03ed227e-6c0d-4065-b020-9d6172bc7df0",
		"name": "NotificationComponent",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "Get All Notifications",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "localhost:3000/api/notifications",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"notifications"
					]
				}
			},
			"response": []
		},
		{
			"name": "Create Notification",
			"request": {
				"method": "POST",
				"header": [
					{
						"key": "Content-Type",
						"name": "Content-Type",
						"value": "application/json",
						"type": "text"
					}
				],
				"body": {
					"mode": "raw",
					"raw": "{\n\t\"title\": \"Test notification\",\n\t\"requirement\": \"Test requirement\"\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "localhost:3000/api/notifications",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"notifications"
					]
				}
			},
			"response": []
		},
		{
			"name": "Check Notification",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": ""
				}
			},
			"response": []
		},
		{
			"name": "Get Bonuses",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "localhost:3000/api/notifications/bonuses",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"notifications",
						"bonuses"
					]
				}
			},
			"response": []
		},
		{
			"name": "Get Promotions",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "localhost:3000/api/notifications/promotions",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"notifications",
						"promotions"
					]
				}
			},
			"response": []
		},
		{
			"name": "Get Texts",
			"request": {
				"method": "GET",
				"header": [],
				"url": {
					"raw": "localhost:3000/api/notifications/texts",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"notifications",
						"texts"
					]
				}
			},
			"response": []
		}
	],
	"protocolProfileBehavior": {}
}